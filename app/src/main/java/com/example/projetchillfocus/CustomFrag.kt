import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.example.projetchillfocus.*
import kotlinx.android.synthetic.main.view_pager_activity.*
import android.app.Activity
import android.widget.LinearLayout
import android.widget.NumberPicker
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.internal.ContextUtils.getActivity
import kotlinx.android.synthetic.main.activity_main.viewPagerPlayer;
import kotlinx.android.synthetic.main.view_pager_activity.*;
import kotlinx.android.synthetic.main.fragment_player.*;
import kotlinx.android.synthetic.main.activity_main.*
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import kotlinx.android.synthetic.main.player_settings.*


class CustomFrag(type : Int) : Fragment() {


    val t = type
    lateinit var bdd:DatabaseHelper
    private lateinit var viewPager: ViewPager
    private lateinit var modelList : ArrayList<Project>
    private lateinit var  adapter: Adapter
    private lateinit var thiscontext : Context
    lateinit var  main: MainActivity
    var activeItem : Int = -12;
    lateinit var timePick : NumberPicker
    lateinit var pausePick : NumberPicker

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        if (container != null) {
            thiscontext = container.getContext()
        };


        bdd=DatabaseHelper(Global.applicationContext())
        main = activity as MainActivity

        if(t == 0){
            val view = inflater.inflate(R.layout.view_pager_activity, container, false)
            viewPager = view.findViewById(R.id.viewPager)

            modelList = bdd.getAllUnfinishedProjects()
           // modelList=ArrayList()
            //Ajoute des cartes


            //Adapter
            adapter = Adapter(context, modelList)
            adapter.setMainActivity(main)
            adapter.setCustomFrag(this)

            //Afficher
            viewPager.adapter = adapter
            viewPager.setPadding(100, 0, 100, 0)
            main.setAdapter(adapter,viewPager)
            if(!modelList.isEmpty())
                main.LoadProject(modelList[0])
            //TODO: get le bon adapter
            viewPager.addOnPageChangeListener(object : OnPageChangeListener {
                override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) {
                    main.setAdapter(adapter,viewPager)
                }

                override fun onPageSelected(position: Int) {
                    if(activeItem >= 0){
                        if(viewPager.currentItem != activeItem)
                        {
                            viewPager.setCurrentItem(activeItem)
                        }
                    }
                    else
                    {
                        main.setAdapter(adapter,viewPager)
                        main.LoadProject(modelList[viewPager.currentItem])
                    }


                }

                override fun onPageScrollStateChanged(state: Int) {
                    main.setAdapter(adapter,viewPager)
                }
            })


            var playerView = view.findViewById<LinearLayout>(R.id.news_title)
            playerView.setOnTouchListener { v, event ->
                when (event?.action) {
                    MotionEvent.ACTION_DOWN -> {
                        (activity as MainActivity?)?.changePagerInput(true)
                    }
                }
                v?.onTouchEvent(event) ?: true
            }

            /*var Button = view?.findViewById(R.id.buttnewproj) as Button
            Button.setOnTouchListener(object : View.OnTouchListener {
                override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                    when (event?.action) {
                        MotionEvent.ACTION_DOWN ->
                        {
                            val intent = Intent(activity, NouveauProj::class.java)
                            startActivity(intent)
                        }

                    }

                    return v?.onTouchEvent(event) ?: true
                }
            })*/

            return view
        }
        else{
            val view = inflater.inflate(R.layout.player_settings, container, false)
            timePick =view.findViewById<NumberPicker>(R.id.timepicker)
            pausePick=view.findViewById<NumberPicker>(R.id.pausepicker)
            main.setupPickers(timePick,pausePick)
            return view
        }

    }

}