package com.example.projetchillfocus
import CustomFrag
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
//import kotlinx.android.synthetic.main.view_pager_activity.*
import android.content.Intent
import android.media.MediaMetadataRetriever
import android.media.MediaPlayer
import android.os.Handler
import android.view.ViewTreeObserver
import android.view.Window
import android.view.WindowManager
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
//import com.sun.security.ntlm.Client

import android.widget.*
import androidx.core.view.doOnLayout
import androidx.viewpager.widget.ViewPager
import kotlinx.android.synthetic.main.player_settings.*
import android.net.Uri







class MainActivity : AppCompatActivity() {
    private lateinit var fraglist: ArrayList<CustomFrag>
    var ArchivePopUp :PopUpArchive =PopUpArchive()
    var OngoingSession:Session? =null
    var activeProj :Project?=null
    lateinit var bdd:DatabaseHelper
    private lateinit var viewPager: ViewPager2
    var appContext = Global.applicationContext() as Global
    private lateinit var playButton: ImageButton
    private var timer = 0

    lateinit var frag1:CustomFrag
    lateinit var frag2 :CustomFrag

    private lateinit var ViewPagerAdapter:Adapter
    private lateinit var cards:ViewPager
    private lateinit var metaRetriever : MediaMetadataRetriever

    var chosenPause :Int=0
    var chosenTime:Int=0

    lateinit var StateText :TextView
    lateinit var TimeText :TextView

    fun setAdapter(ada:Adapter, cardview: ViewPager)
    {
        ViewPagerAdapter=ada
        cards=cardview
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let { onRestoreInstanceState(it) }
        supportActionBar?.hide()

        setContentView(R.layout.activity_main)

        viewPager = findViewById(R.id.viewPagerPlayer)
        bdd=DatabaseHelper(appContext)
        chosenPause =bdd.getDefaultSettings().pauses
        chosenTime=bdd.getDefaultSettings().sessionDuration
        launchAdapter()
        viewPager.isUserInputEnabled = false

        viewPager.doOnLayout {
            playButton=findViewById(R.id.play_pause)
            StateText=findViewById<TextView>(R.id.sessionstate)
            TimeText=findViewById<TextView>(R.id.timer_view)

        }


    }


    fun setupPickers(timePicker: NumberPicker,pausePicker: NumberPicker)
    {


        pausePicker.minValue=0
        pausePicker.maxValue=10
        timePicker.minValue = 1
        timePicker.maxValue = 60


        pausePicker.value=bdd.getDefaultSettings().pauses
        timePicker.value=bdd.getDefaultSettings().sessionDuration


        pausePicker.setOnValueChangedListener { picker, oldVal, newVal ->
            chosenPause=newVal
        }
        timePicker.setOnValueChangedListener { picker, oldVal, newVal ->
            chosenTime=newVal
        }





    }


    public fun changePagerInput(input : Boolean){
        viewPager.isUserInputEnabled = input;
    }

    override fun onStart() {
        super.onStart()
        RestoreSession()

    }

    override fun onStop() {
        super.onStop()
        if(OngoingSession?.State==SessionState.ONGOING)
        {
            pauseSession(false)
            endSession()
        }

        handler.removeCallbacks(runnable)

    }


    fun RestoreSession()
    {
        OngoingSession=appContext.getSession()
        initPlayer()
        if (OngoingSession!=null)
        {
            CurrentSong=OngoingSession!!.LastTrackNum
            initPlayer()
            player?.seekTo(OngoingSession!!.Timestamp)
            activeProj=OngoingSession!!.proj
            var list:ArrayList<Project> = bdd.getAllUnfinishedProjects()


            viewPager.doOnLayout {
                cards.setCurrentItem(list.indexOf(activeProj))
                ViewPagerAdapter.animateDown() //TODO: ADAPTER PROBLEM Wrong tape is going down
                timer=OngoingSession!!.ElapsedTime
                StateText.text = "Taking a break"
                val hours = timer / 3600
                val minutes = timer % 3600 / 60
                val secs = timer % 60
                val time = String.format("%d:%02d:%02d",
                    hours, minutes, secs)


                TimeText.text = time
            }
        }
        runTimer()
    }


    fun OpenMenu(view: View)
    {
        if(OngoingSession?.State==SessionState.ONGOING) {
            pauseSession(true)
        }
        else {
            val intent = Intent(this@MainActivity, Menu::class.java)
            startActivity(intent)
        }
    }
    fun NewProj(view:View)
    {
        if(OngoingSession?.State==SessionState.ONGOING) {
            pauseSession(true)
        }
        else
        {
            val intent = Intent(this@MainActivity, NouveauProj::class.java)
            startActivity(intent)
        }
    }
    fun ArchivePopUp(v:View)
    {
        ArchivePopUp.show(supportFragmentManager,"Archive")
    }
    fun ArchiveProj()
    {
        activeProj?.isFinished=true;
        bdd.updateProject(activeProj!!)
        activeProj=null
        launchAdapter()
    }
    fun DeleteProj()
    {
        bdd.deleteProject(activeProj!!)
        activeProj=null
        launchAdapter()

    }

    fun endSession()
    {
        StateText.text="No work started yet"
        TimeText.text=""

        ViewPagerAdapter.animateUp()

        if(OngoingSession!=null) {
            if (OngoingSession!!.ElapsedTime < OngoingSession!!.ChosenTime) {
                activeProj!!.stoppedSessions += 1
                Toast.makeText(this@MainActivity,"Session failed...",Toast.LENGTH_SHORT).show()
            } else {
                activeProj!!.finishedSessions += 1
                Toast.makeText(this@MainActivity,"Session successful! Congrats!",Toast.LENGTH_SHORT).show()
            }
            activeProj!!.hours+=OngoingSession!!.ElapsedTime
            bdd.updateProject(activeProj!!)
        }

        OngoingSession=null
        appContext.saveSession(OngoingSession)
    }
    fun pauseSession(withPop:Boolean)
    {
        if(OngoingSession!=null) {

            StateText.text = "Taking a break"

            PauseMusic()

            OngoingSession!!.State = SessionState.PAUSE
            OngoingSession!!.Timestamp = player!!.currentPosition
            OngoingSession!!.LastTrackNum = CurrentSong
            OngoingSession!!.ElapsedTime = timer
            OngoingSession!!.let { appContext.saveSession(it) }



            if (withPop) PausePopUp(OngoingSession!!.ElapsedTime<OngoingSession!!.ChosenTime).show(supportFragmentManager, "PausePopUp")


        }
    }
    fun Remove1Break() //called only when pause is clicked on the popup
    {

        OngoingSession!!.State=SessionState.PAUSE
        ViewPagerAdapter.animateDown()
        OngoingSession!!.RemainingBreaks-=1
    }
    fun resumeSession()
    {

        if(OngoingSession?.State==SessionState.PAUSE )
        {
            StateText.text=activeProj?.name


            PlayMusic()
            OngoingSession?.State=SessionState.ONGOING
            timer = OngoingSession!!.ElapsedTime
        }
        else if (OngoingSession==null)
            startSession()


    }
    fun startSession()
    {
        if (activeProj!=null) {
            StateText.text=activeProj?.name
            //TODO ahahaha settings
            var Set :Settings =bdd.getDefaultSettings()
            ViewPagerAdapter.animateDown()
            OngoingSession = Session(
                0,
                0,
                SessionState.ONGOING,

                chosenPause,
                0,
                chosenTime*60,
                activeProj!!
            )

            timer = 0
            PlayMusic()
        }
        else
        {
            Toast.makeText(this@MainActivity, "No project is active!", Toast.LENGTH_SHORT).show()
        }

    }
    private fun launchAdapter(){
        fraglist = ArrayList()

        frag1=CustomFrag(1)
        frag2=CustomFrag(2)

        fraglist.add(frag1)
        fraglist.add(frag2)


        val fragAdapter = ScreenSlidePagerAdapter(this)
        viewPager.adapter = fragAdapter
    }

    private inner class ScreenSlidePagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {
        override fun getItemCount(): Int = 2

        override fun createFragment(position: Int): Fragment = CustomFrag(position)
    }

    override fun onBackPressed() {
        if (viewPager.currentItem == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed()
        } else {
            // Otherwise, select the previous step.
            viewPager.currentItem = viewPager.currentItem - 1
        }
    }

    fun LoadProject(proj:Project)
    {
        activeProj=proj
        //TODO: get setting of the project and put them in variable et des qu'on change les picker on change les variables
    }

    ///MUSIC PLAYER
    private var MusicList: ArrayList<Int> = ArrayList()
    var player: MediaPlayer? = null
    var CurrentSong : Int = 0
    var isPlaying = false
    enum class LoopState {
        NONE, LOOPMANY, LOOPONE
    }

    var loopState: LoopState = LoopState.NONE

    fun initPlayer()
    {
        initMusicList()
        player=MediaPlayer.create(this, MusicList[CurrentSong])
        player?.isLooping = false
        player?.setVolume(100f,100f)

        player!!.setOnCompletionListener(object : MediaPlayer.OnCompletionListener {
            override fun onCompletion(mp: MediaPlayer?) {
                NexMu()
            }

        })

    }

    fun initMusicList()
    {
        MusicList.add(R.raw.music1)
        MusicList.add(R.raw.music2)
        MusicList.add(R.raw.music3)
        MusicList.add(R.raw.music4)
        MusicList.add(R.raw.music5)
        MusicList.add(R.raw.music6)
        MusicList.add(R.raw.music7)
        MusicList.add(R.raw.music8)
        MusicList.add(R.raw.music9)
    }

    fun PlayPause()
    {
        if(player!!.isPlaying){
            pauseSession(true)
        } else {
            resumeSession()
        }
    }
    fun PlayMusic()
    {

        player?.start()
        playButton.setImageResource(R.drawable.pause)
        isPlaying=true
    }
    fun PauseMusic()
    {

        playButton.setImageResource(R.drawable.play)
        player?.pause()
        isPlaying=false
    }

    fun NexMu()
    {
        player?.pause()
        if(CurrentSong<MusicList.size-1)
            CurrentSong++
        else if(loopState==LoopState.LOOPMANY)
        {
            CurrentSong=0
        }

        player= MediaPlayer.create(this, MusicList[CurrentSong])
        if(isPlaying)
            player?.start()
    }

    fun PrevMu()
    {

        player?.pause()
        if(CurrentSong>0)
        {
            CurrentSong--
        }else if(loopState==LoopState.LOOPMANY)
            CurrentSong=MusicList.size-1

        player= MediaPlayer.create(this, MusicList[CurrentSong])
        if(isPlaying)
            player?.start()
    }

    fun Play(view: View)
    {
        PlayPause()
    }
    fun PreviousMusic(view: View)
    {
        PrevMu()
    }
    fun NextMusic(view: View)
    {
        NexMu()
    }

    fun OnLoop()
    {
        when(loopState)
        {
            LoopState.NONE ->
            {
                loopState= LoopState.LOOPMANY
                player?.isLooping=false
            }
            LoopState.LOOPMANY ->
            {
                loopState= LoopState.LOOPONE
                player?.isLooping=true
            }
            LoopState.LOOPONE ->
            {
                loopState= LoopState.NONE
                player?.isLooping=false
            }
        }
    }

    lateinit var handler :Handler
    lateinit var runnable :Runnable
    private fun runTimer() {


        //java text view associated with the xml one
        handler = Handler()
        runnable = Runnable {
                val hours = timer / 3600
                val minutes = timer % 3600 / 60
                val secs = timer % 60
                val time = String.format("%d:%02d:%02d",
                    hours, minutes, secs)
                if (isPlaying) {

                    TimeText.text = time
                    timer++
                    if(timer + OngoingSession!!.ElapsedTime > OngoingSession!!.ChosenTime){
                        //Session Réussie
                    }
                }
                handler.postDelayed(runnable, 1000)
            }
        handler.post(runnable)
    }

}

