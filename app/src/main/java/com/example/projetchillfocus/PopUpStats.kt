package com.example.projetchillfocus

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment

class PopUpStats(var projectId: Int): DialogFragment() {
    lateinit var act: Activity
    private lateinit var db: DatabaseHelper

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        act = activity as Activity
        db = DatabaseHelper(act.applicationContext)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        act = activity as Activity
        db = DatabaseHelper(act.applicationContext)
        val project: Project = db.getProject(projectId)
        return activity?.let {
            // Use the Builder class for convenient dialog construction
            val builder = AlertDialog.Builder(it)
            builder.setMessage("Number of hours: "+project.numberHours()+
                    "\nNumber of sessions: "+project.sessions()+
                    "\nNumber of pauses: "+project.pauses+
                    "\nNumber of successful sessions: "+project.finishedSessions+
                    "\nNumber of failed sessions: "+project.stoppedSessions)
                .setIcon(R.drawable.icon)
                .setTitle(project.name)
                .setNeutralButton("Back",
                    DialogInterface.OnClickListener { dialog, id ->
                        dismiss()
                    })
            // Create the AlertDialog object and return it
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}