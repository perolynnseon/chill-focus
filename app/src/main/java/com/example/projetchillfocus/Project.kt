package com.example.projetchillfocus

import android.os.Build
import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.RequiresApi

class Project(var id: Int,
              var name: String,
              var color: String,
              var description: String = "",
              var hasDefaultSettings: Boolean = false,
              var image: Int = R.drawable.tape1,
              var isFinished: Boolean = false,
              var hours: Int = 0,
              var finishedSessions: Int = 0,
              var stoppedSessions: Int = 0,
              var pauses: Int = 0,
              var idSettings: Int = 0): Parcelable {

    @RequiresApi(Build.VERSION_CODES.Q)
    constructor(parcel: Parcel) : this(

        parcel.readInt(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readBoolean(),
        parcel.readInt(),
        parcel.readBoolean(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt()


    ) {
    }


    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeInt(image)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Project> {
        @RequiresApi(Build.VERSION_CODES.Q)
        override fun createFromParcel(parcel: Parcel): Project {
            return Project(parcel)
        }

        override fun newArray(size: Int): Array<Project?> {
            return arrayOfNulls(size)
        }
    }

    fun sessions(): Int { return finishedSessions + stoppedSessions }
    fun numberHours(): String { return (hours/3600).toString() + "h"+(hours % 3600 / 60)+"min"+(hours % 60)+"s"}
}