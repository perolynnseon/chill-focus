package com.example.projetchillfocus

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.json.JSONStringer
import java.lang.Exception

class Bibliotheque : AppCompatActivity() {
    private lateinit var db: DatabaseHelper
    private lateinit var recyclerView: RecyclerView
    private lateinit var listProjects: ArrayList<Project>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.bibliotheque)

        db = DatabaseHelper(applicationContext)
        recyclerView = findViewById<RecyclerView>(R.id.finished_projects_list)
        listProjects = db.getAllFinishedProjects()    // TODO: changer pour finished projects

        var globalClass = Global.applicationContext() as Global


        var listTitle:ArrayList<String> = ArrayList()
        for (i in 0 until listProjects.size) {
            listTitle.add(listProjects[i].name)
            Log.d("MOI",listProjects[i].name)
        }

        //val adapter = ArrayAdapter(this, R.layout.finished_project_item, listTitle)
        val layoutManager = LinearLayoutManager(applicationContext)
        val adapter = BibliothequeAdapter(listProjects)
        adapter.setActivity(this)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter

    }

    fun BackToMenu(view: View)
    {
        val intent = Intent(this@Bibliotheque, Menu::class.java)
        startActivity(intent)
    }
}