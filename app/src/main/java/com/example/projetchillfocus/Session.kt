package com.example.projetchillfocus

enum class SessionState
{
    ONGOING,PAUSE
}
class Session constructor(
    public var LastTrackNum:Int=0,
    public var Timestamp:Int=0,
    public var State:SessionState=SessionState.ONGOING,
    public var RemainingBreaks :Int=0,
    public var ElapsedTime :Int=0,
    public var ChosenTime :Int=0,
    public var proj:Project)
{
}