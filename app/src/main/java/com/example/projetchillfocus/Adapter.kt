package com.example.projetchillfocus

import CustomFrag
import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import kotlinx.android.synthetic.main.card_item.view.*
import kotlinx.android.synthetic.main.activity_main.viewPagerPlayer
import kotlinx.android.synthetic.main.view_pager_activity.*


class Adapter(private val context: Context?, private val modelArrayList: ArrayList<Project>) : PagerAdapter() {
    var yDown : Float = 0.0f
    var yBackup : Float = 0.0f
    var move : Float = 0.0f;
    var maxMove : Float = 550f
    lateinit var main : MainActivity;
    lateinit var customFraggg : CustomFrag;
    lateinit var activeView :View

    fun setMainActivity(activity: MainActivity){
        main = activity
    }

    fun setCustomFrag(frag : CustomFrag){
        customFraggg = frag;
    }

    override fun getCount(): Int {
        return modelArrayList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    fun clamp(`val`: Float, min: Float, max: Float): Float {
        return Math.max(min, Math.min(max, `val`))
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(context).inflate(R.layout.card_item, container, false)
        val model = modelArrayList[position]
        // TODO rajouter d'autres valeurs à get si besoin
        val title = model.name
        val image = model.image
        var textTapeView = view.findViewById<TextView>(R.id.textTape)
        textTapeView.text = title
        //Set data to ui view
        view.bannerIv.setImageResource(image)

        //Click
        view.setOnClickListener(){
            //PopUpStats(model.id).show(main.supportFragmentManager,"Stats")
        }

        container.addView(view, position)
        activeView=view
        view.setOnTouchListener { v, event ->
            when (event?.action) {
                MotionEvent.ACTION_DOWN -> {
                    yDown = view.getY() - event.getRawY()
                    yBackup = view.getY()
                    main.changePagerInput(false)
                }
                MotionEvent.ACTION_MOVE -> {
                    move = clamp(event.getRawY() + yDown, 0f, maxMove)
                    view.animate()
                        .y(move)
                        .setDuration(0)
                        .start()
                }
                MotionEvent.ACTION_UP -> {
                    activeView=view
                    if (event.getRawY() + yDown < (maxMove / 1.2)) {
                        animateUp()
                        main.pauseSession(true)
                    } else {
                        animateDown()
                        main.resumeSession()

                    }
                }
            }
            v?.onTouchEvent(event) ?: true
        }

        return view
    }

    fun animateUp()
    {
        customFraggg.activeItem = -12
        activeView.animate()
            .y(0f)
            .setDuration(0)
            .start()

    }

    fun animateDown()
    {
        customFraggg.activeItem = customFraggg.viewPager.currentItem
        activeView.animate()
            .y(maxMove)
            .setDuration(0)
            .start()

    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}