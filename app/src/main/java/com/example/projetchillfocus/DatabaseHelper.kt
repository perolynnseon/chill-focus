package com.example.projetchillfocus

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log

class DatabaseHelper(context: Context): SQLiteOpenHelper(context,DATABASE_NAME,null,DATABASE_VERSION) {
    // Database information
    companion object {
        val LOG = "DatabaseHelper"
        val DATABASE_VERSION = 1
        val DATABASE_NAME = "ProjectsDatabase"
    }

    // Table names
    private val TABLE_PROJECT = "Project"
    private val TABLE_MUSIC = "Music"
    private val TABLE_SETTINGS = "Settings"
    private val TABLE_LINK_PROJECT_MUSIC = "LinkProjectMusic"

    // Common column names
    private val KEY_ID = "id"
    private val KEY_PAUSES = "pauses"

    // Project column names
    private val KEY_NAME = "name"
    private val KEY_COLOR = "color"
    private val KEY_DESCRIPTION = "description"
    private val KEY_HAS_DEFAULT_SETTINGS = "hasDefaultSettings"
    private val KEY_IS_FINISHED = "isFinished"
    private val KEY_HOURS = "hours"
    private val KEY_FINISHED_SESSIONS = "finishedSessions"
    private val KEY_STOPPED_SESSIONS = "stoppedSessions"
    private val KEY_IMAGE = "image"
    private val KEY_ID_SETTINGS = "idSettings"

    // Music column names
    private val KEY_FILE_NAME = "fileName"
    private val KEY_RESSOURCE = "ressource"

    // Settings column names
    private val KEY_SESSION_DURATION = "sessionDuration"
    private val KEY_IS_DEFAULT = "isDefault"

    // LinkProjectMusic column names
    private val KEY_PROJECT_ID = "idProject"
    private val KEY_MUSIC_ID = "idMusic"

    // Table create statements
    // Project table create statement
    private val CREATE_TABLE_PROJECT = "CREATE TABLE $TABLE_PROJECT(" +
            "$KEY_ID INTEGER PRIMARY KEY," +
            "$KEY_NAME TEXT NOT NULL," +
            "$KEY_COLOR TEXT," +
            "$KEY_DESCRIPTION TEXT," +
            "$KEY_HAS_DEFAULT_SETTINGS INTEGER," +
            "$KEY_IS_FINISHED INTEGER," +
            "$KEY_HOURS INTEGER," +
            "$KEY_FINISHED_SESSIONS INTEGER," +
            "$KEY_STOPPED_SESSIONS INTEGER," +
            "$KEY_PAUSES INTEGER," +
            "$KEY_IMAGE INTEGER," +
            "$KEY_ID_SETTINGS INTEGER," +
            "FOREIGN KEY ($KEY_ID_SETTINGS) REFERENCES $TABLE_SETTINGS($KEY_ID))"

    // Music table create statement
    private val CREATE_TABLE_MUSIC = "CREATE TABLE $TABLE_MUSIC(" +
            "$KEY_ID INTEGER PRIMARY KEY," +
            "$KEY_FILE_NAME TEXT," +
            "$KEY_RESSOURCE INTEGER)"

    // Settings table create statement
    private val CREATE_TABLE_SETTINGS = "CREATE TABLE $TABLE_SETTINGS(" +
            "$KEY_ID INTEGER PRIMARY KEY," +
            "$KEY_PAUSES INTEGER," +
            "$KEY_SESSION_DURATION INTEGER," +
            "$KEY_IS_DEFAULT INTEGER)"

    // LinkProjectMusic table create statement
    private val CREATE_TABLE_LINK_PROJECT_MUSIC = "CREATE TABLE $TABLE_LINK_PROJECT_MUSIC(" +
            "$KEY_ID INTEGER PRIMARY KEY," +
            "$KEY_PROJECT_ID INTEGER," +
            "$KEY_MUSIC_ID INTEGER," +
            "FOREIGN KEY ($KEY_PROJECT_ID) REFERENCES $TABLE_PROJECT($KEY_ID)," +
            "FOREIGN KEY ($KEY_MUSIC_ID) REFERENCES $TABLE_MUSIC($KEY_ID))"

    override fun onCreate(db: SQLiteDatabase) {
        // creating required tables
        db.execSQL(CREATE_TABLE_PROJECT)
        db.execSQL(CREATE_TABLE_MUSIC)
        db.execSQL(CREATE_TABLE_LINK_PROJECT_MUSIC)
        db.execSQL(CREATE_TABLE_SETTINGS)
        //db.close()
        //create default settings with 0 pause and a session duration of 1 hour
        //addSettings(Settings(0, 0, 60, true))
        InitSettings(Settings(0, 0, 60, true),db)
        Log.d(LOG, "Database created.")
    }

    fun InitSettings(settings: Settings, db: SQLiteDatabase): Int
    {
        val values = ContentValues()
        values.put(KEY_PAUSES, settings.pauses)
        values.put(KEY_SESSION_DURATION, settings.sessionDuration)
        if (settings.isDefault) values.put(KEY_IS_DEFAULT, 1)
        else values.put(KEY_IS_DEFAULT, 0)

        // insert row
        val id = db.insert(TABLE_SETTINGS, null, values)
        //db.close()

        Log.d(LOG, "Settings added to the database.")

        return id.toInt()
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS $TABLE_PROJECT")
        db.execSQL("DROP TABLE IF EXISTS $TABLE_MUSIC")
        db.execSQL("DROP TABLE IF EXISTS $TABLE_LINK_PROJECT_MUSIC")
        db.execSQL("DROP TABLE IF EXISTS $TABLE_SETTINGS")

        // create new tables
        onCreate(db)

        Log.d(LOG, "Database updated.")
    }

    // Add a project
    fun addProject(project: Project): Int {
        val db = this.writableDatabase
        val values = ContentValues()

        values.put(KEY_NAME, project.name)
        values.put(KEY_COLOR, project.color)
        values.put(KEY_DESCRIPTION, project.description)
        if (project.isFinished) values.put(KEY_IS_FINISHED, 1)
        else values.put(KEY_IS_FINISHED, 0)
        if (project.hasDefaultSettings) values.put(KEY_HAS_DEFAULT_SETTINGS, 1)
        else values.put(KEY_HAS_DEFAULT_SETTINGS, 0)
        values.put(KEY_HOURS, project.hours)
        values.put(KEY_FINISHED_SESSIONS, project.finishedSessions)
        values.put(KEY_STOPPED_SESSIONS, project.stoppedSessions)
        values.put(KEY_PAUSES, project.pauses)
        values.put(KEY_IMAGE, project.image)
        if (project.hasDefaultSettings) values.put(KEY_ID_SETTINGS, getDefaultSettings().id)
        else if (project.idSettings != null) values.put(KEY_ID_SETTINGS, project.idSettings)

        // insert row
        val id = db.insert(TABLE_PROJECT, null, values)
        //db.close()

        Log.d(LOG, "Project ${project.name} added to the database.")

        return id.toInt()
    }

    // Add a music
    fun addMusic(music: Music): Int {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(KEY_FILE_NAME, music.fileName)
        values.put(KEY_RESSOURCE, music.ressource)

        // insert row
        val id = db.insert(TABLE_MUSIC, null, values)
        //db.close()

        Log.d(LOG, "Music ${music.fileName} added to the database.")

        return id.toInt()
    }

    // Add settings
    private fun addSettings(settings: Settings): Int {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(KEY_PAUSES, settings.pauses)
        values.put(KEY_SESSION_DURATION, settings.sessionDuration)
        if (settings.isDefault) values.put(KEY_IS_DEFAULT, 1)
        else values.put(KEY_IS_DEFAULT, 0)

        // insert row
        val id = db.insert(TABLE_SETTINGS, null, values)
        //db.close()

        Log.d(LOG, "Settings added to the database.")

        return id.toInt()
    }

    // Add all musics to the database
    fun addAllMusics(musics: ArrayList<Int>) {
        //musics.forEach { addMusic(Music(0, /* récup du nom du fichier */, it)) }
    }

    // Add a link between a music and a project
    private fun addLinkProjectMusic(project: Project, music: Music): Int {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(KEY_PROJECT_ID, project.id)
        values.put(KEY_MUSIC_ID, music.id)

        // insert row
        val id = db.insert(TABLE_MUSIC, null, values)
        //db.close()

        Log.d(LOG, "Link between project ${project.name} and music ${music.fileName} added to the database.")

        return id.toInt()
    }

    // Add musics to a project
    fun addMusicsToProject(project: Project, musics: ArrayList<Music>) {
        musics.forEach { addLinkProjectMusic(project, it) }
        Log.d(LOG, "All musics added for project ${project.name}.")
    }

    // Add settings to a project
    fun addSettingsToProject(project: Project, settings: Settings) {
        val id: Int = addSettings(settings)
        project.idSettings = id
        updateProject(project)
        Log.d(LOG, "Settings added for project ${project.name}.")
    }

    // Get a project by id
    @SuppressLint("Range")
    fun getProject(id: Int): Project {
        val selectQuery = "SELECT * FROM $TABLE_PROJECT WHERE $KEY_ID = $id"

        val db = this.readableDatabase
        val cursor = db.rawQuery(selectQuery, null)

        // looping through all rows and adding to list
        if (cursor != null) cursor.moveToFirst()

        val name = cursor.getString(cursor.getColumnIndex(KEY_NAME))
        val color = cursor.getString(cursor.getColumnIndex(KEY_COLOR))
        val description = cursor.getString(cursor.getColumnIndex(KEY_DESCRIPTION))
        val isFinished = cursor.getInt(cursor.getColumnIndex(KEY_IS_FINISHED)) > 0
        val hasDefaultSettings = cursor.getInt(cursor.getColumnIndex(KEY_HAS_DEFAULT_SETTINGS)) > 0
        val hours = cursor.getInt(cursor.getColumnIndex(KEY_HOURS))
        val finishedSessions = cursor.getInt(cursor.getColumnIndex(KEY_FINISHED_SESSIONS))
        val stoppedSessions = cursor.getInt(cursor.getColumnIndex(KEY_STOPPED_SESSIONS))
        val pauses = cursor.getInt(cursor.getColumnIndex(KEY_PAUSES))
        val image = cursor.getInt(cursor.getColumnIndex(KEY_IMAGE))

        cursor.close()
        //db.close()
        Log.d(LOG, "Project $name fetched from the database.")

        return Project(id, name, color, description, hasDefaultSettings, image, isFinished, hours, finishedSessions, stoppedSessions, pauses)
    }

    // Get all the unfinished projects
    @SuppressLint("Range")
    fun getAllUnfinishedProjects(): ArrayList<Project> {
        val projects: ArrayList<Project> = ArrayList<Project>()
        val selectQuery = "SELECT * FROM $TABLE_PROJECT WHERE $KEY_IS_FINISHED = 0"

        val db = this.readableDatabase
        val cursor = db.rawQuery(selectQuery, null)

        var id: Int
        var name: String
        var color: String
        var description: String
        var isFinished: Boolean
        var hasDefaultSettings: Boolean
        var hours: Int
        var finishedSessions: Int
        var stoppedSessions: Int
        var pauses: Int
        var image: Int

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                id = cursor.getInt(cursor.getColumnIndex(KEY_ID))
                name = cursor.getString(cursor.getColumnIndex(KEY_NAME))
                color = cursor.getString(cursor.getColumnIndex(KEY_COLOR))
                description = cursor.getString(cursor.getColumnIndex(KEY_DESCRIPTION))
                isFinished = cursor.getInt(cursor.getColumnIndex(KEY_IS_FINISHED)) > 0
                hasDefaultSettings = cursor.getInt(cursor.getColumnIndex(KEY_HAS_DEFAULT_SETTINGS)) > 0
                hours = cursor.getInt(cursor.getColumnIndex(KEY_HOURS))
                finishedSessions = cursor.getInt(cursor.getColumnIndex(KEY_FINISHED_SESSIONS))
                stoppedSessions = cursor.getInt(cursor.getColumnIndex(KEY_STOPPED_SESSIONS))
                pauses = cursor.getInt(cursor.getColumnIndex(KEY_PAUSES))
                image = cursor.getInt(cursor.getColumnIndex(KEY_IMAGE))

                val tmp = Project(id, name, color, description, hasDefaultSettings, image, isFinished, hours, finishedSessions, stoppedSessions, pauses)
                projects.add(tmp)
            } while (cursor.moveToNext())
        }

        cursor.close()
        //db.close()

        Log.d(LOG, "All unfinished projects fetched from the database.")

        return projects
    }

    // Get all the finished projects
    @SuppressLint("Range")
    fun getAllFinishedProjects(): ArrayList<Project> {
        val projects: ArrayList<Project> = ArrayList<Project>()
        val selectQuery = "SELECT * FROM $TABLE_PROJECT WHERE $KEY_IS_FINISHED > 0"

        val db = this.readableDatabase
        val cursor = db.rawQuery(selectQuery, null)

        var id: Int
        var name: String
        var color: String
        var description: String
        var isFinished: Boolean
        var hasDefaultSettings: Boolean
        var hours: Int
        var finishedSessions: Int
        var stoppedSessions: Int
        var pauses: Int
        var image: Int

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                id = cursor.getInt(cursor.getColumnIndex(KEY_ID))
                name = cursor.getString(cursor.getColumnIndex(KEY_NAME))
                color = cursor.getString(cursor.getColumnIndex(KEY_COLOR))
                description = cursor.getString(cursor.getColumnIndex(KEY_DESCRIPTION))
                isFinished = cursor.getInt(cursor.getColumnIndex(KEY_IS_FINISHED)) > 0
                hasDefaultSettings = cursor.getInt(cursor.getColumnIndex(KEY_HAS_DEFAULT_SETTINGS)) > 0
                hours = cursor.getInt(cursor.getColumnIndex(KEY_HOURS))
                finishedSessions = cursor.getInt(cursor.getColumnIndex(KEY_FINISHED_SESSIONS))
                stoppedSessions = cursor.getInt(cursor.getColumnIndex(KEY_STOPPED_SESSIONS))
                pauses = cursor.getInt(cursor.getColumnIndex(KEY_PAUSES))
                image = cursor.getInt(cursor.getColumnIndex(KEY_IMAGE))

                val tmp = Project(id, name, color, description, hasDefaultSettings, image, isFinished, hours, finishedSessions, stoppedSessions, pauses)
                projects.add(tmp)
            } while (cursor.moveToNext())
        }

        cursor.close()
        //db.close()

        Log.d(LOG, "All finished projects fetched from the database.")

        return projects
    }

    // Get all the musics
    @SuppressLint("Range")
    fun getAllMusics(): ArrayList<Music> {
        val musics: ArrayList<Music> = ArrayList<Music>()
        val selectQuery = "SELECT * FROM $TABLE_MUSIC"

        val db = this.readableDatabase
        val cursor = db.rawQuery(selectQuery, null)

        var id: Int
        var fileName: String
        var ressource: Int

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                id = cursor.getInt(cursor.getColumnIndex(KEY_ID))
                fileName = cursor.getString(cursor.getColumnIndex(KEY_FILE_NAME))
                ressource = cursor.getInt(cursor.getColumnIndex(KEY_RESSOURCE))

                val tmp = Music(id, fileName, ressource)
                musics.add(tmp)
            } while (cursor.moveToNext())
        }

        cursor.close()
        //db.close()

        Log.d(LOG, "All musics fetched from the database.")

        return musics
    }

    // Get all the musics of a project
    @SuppressLint("Range")
    fun getProjectMusics(project: Project): ArrayList<Music> {
        val musics: ArrayList<Music> = ArrayList<Music>()
        val selectQuery = "SELECT $TABLE_MUSIC.* " +
            "FROM $TABLE_LINK_PROJECT_MUSIC INNER JOIN $TABLE_MUSIC ON $TABLE_LINK_PROJECT_MUSIC.$KEY_MUSIC_ID = $TABLE_MUSIC.$KEY_ID " +
            "WHERE $KEY_PROJECT_ID = ${project.id}"

        val db = this.readableDatabase
        val cursor = db.rawQuery(selectQuery, null)

        var id: Int
        var fileName: String
        var ressource: Int

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                id = cursor.getInt(cursor.getColumnIndex(KEY_ID))
                fileName = cursor.getString(cursor.getColumnIndex(KEY_FILE_NAME))
                ressource = cursor.getInt(cursor.getColumnIndex(KEY_RESSOURCE))

                val tmp = Music(id, fileName, ressource)
                musics.add(tmp)
            } while (cursor.moveToNext())
        }

        cursor.close()
        //db.close()

        Log.d(LOG, "All musics of project ${project.name} fetched from the database.")

        return musics
    }

    // Get settings by id
    @SuppressLint("Range")
    fun getSettingsById(id: Int): Settings {
        val selectQuery = "SELECT * FROM $TABLE_SETTINGS WHERE $KEY_ID = $id"

        val db = this.readableDatabase
        val cursor = db.rawQuery(selectQuery, null)

        // looping through all rows and adding to list
        if (cursor != null) cursor.moveToFirst()

        val id = cursor.getInt(cursor.getColumnIndex(KEY_ID))
        val pauses = cursor.getInt(cursor.getColumnIndex(KEY_PAUSES))
        val sessionDuration = cursor.getInt(cursor.getColumnIndex(KEY_SESSION_DURATION))
        val isDefault = cursor.getInt(cursor.getColumnIndex(KEY_IS_DEFAULT)) > 0

        cursor.close()
        //db.close()

        Log.d(LOG, "Settings fetched from the database.")

        return Settings(id, pauses, sessionDuration, isDefault)
    }

    // Get default settings
    @SuppressLint("Range")
    fun getDefaultSettings(): Settings {
        val selectQuery = "SELECT * FROM $TABLE_SETTINGS WHERE $KEY_IS_DEFAULT > 0"

        val db = this.readableDatabase
        val cursor = db.rawQuery(selectQuery, null)

        // looping through all rows and adding to list
        if (cursor != null) cursor.moveToFirst()

        val id = cursor.getInt(cursor.getColumnIndex(KEY_ID))
        val pauses = cursor.getInt(cursor.getColumnIndex(KEY_PAUSES))
        val sessionDuration = cursor.getInt(cursor.getColumnIndex(KEY_SESSION_DURATION))
        val isDefault = cursor.getInt(cursor.getColumnIndex(KEY_IS_DEFAULT)) > 0

        cursor.close()
        //db.close()

        Log.d(LOG, "Default settings fetched from the database.")

        return Settings(id, pauses, sessionDuration, isDefault)
    }

    // Update project
    fun updateProject(project: Project): Int {
        val db = this.writableDatabase
        val values = ContentValues()

        values.put(KEY_ID, project.id)
        values.put(KEY_NAME, project.name)
        values.put(KEY_COLOR, project.color)
        values.put(KEY_DESCRIPTION, project.description)
        if (project.isFinished) values.put(KEY_IS_FINISHED, 1)
        else values.put(KEY_IS_FINISHED, 0)
        if (project.hasDefaultSettings) values.put(KEY_HAS_DEFAULT_SETTINGS, 1)
        else values.put(KEY_HAS_DEFAULT_SETTINGS, 0)
        values.put(KEY_HOURS, project.hours)
        values.put(KEY_FINISHED_SESSIONS, project.finishedSessions)
        values.put(KEY_STOPPED_SESSIONS, project.stoppedSessions)
        values.put(KEY_PAUSES, project.pauses)
        values.put(KEY_IMAGE, project.image)
        if (project.hasDefaultSettings) values.put(KEY_ID_SETTINGS, getDefaultSettings().id)
        else if (project.idSettings != null) values.put(KEY_ID_SETTINGS, project.idSettings)

        // update row
        val id = db.update(TABLE_PROJECT, values, "$KEY_ID = ?", arrayOf(project.id.toString()))
        //db.close()

        Log.d(LOG, "Project ${project.name} updated.")

        return id
    }

    // Update a music
    fun updateMusic(music: Music): Int {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(KEY_ID, music.id)
        values.put(KEY_FILE_NAME, music.fileName)
        values.put(KEY_RESSOURCE, music.ressource)

        // update row
        val id = db.update(TABLE_MUSIC, values, "$KEY_ID = ?", arrayOf(music.id.toString()))
        //db.close()

        Log.d(LOG, "Music ${music.fileName} updated.")

        return id
    }

    // Update settings
    fun updateSettings(settings: Settings): Int {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(KEY_ID, settings.id)
        values.put(KEY_PAUSES, settings.pauses)
        values.put(KEY_SESSION_DURATION, settings.sessionDuration)
        if (settings.isDefault) values.put(KEY_IS_DEFAULT, 1)
        else values.put(KEY_IS_DEFAULT, 0)

        // update row
        val id = db.update(TABLE_SETTINGS, values, "$KEY_ID = ?", arrayOf(settings.id.toString()))
        //db.close()

        Log.d(LOG, "Settings updated.")

        return id
    }

    // Update default settings
    fun updateDefaultSettings(settings: Settings): Int {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(KEY_PAUSES, settings.pauses)
        values.put(KEY_SESSION_DURATION, settings.sessionDuration)
        values.put(KEY_IS_DEFAULT, 1)

        // update row
        val id = db.update(TABLE_SETTINGS, values, "$KEY_IS_DEFAULT > ?", arrayOf("0"))
        //db.close()

        Log.d(LOG, "Default settings updated.")

        return id
    }

    // Delete a project
    fun deleteProject(project: Project) {
        deleteLinkWithProjectId(project.id)
        val db = this.writableDatabase
        db.delete(TABLE_PROJECT, "$KEY_ID = ?", arrayOf(project.id.toString()))
        if (!project.hasDefaultSettings && project.idSettings != null) {
            deleteSettings(project.idSettings!!)
        }
        //db.close()

        Log.d(LOG, "Project ${project.name} deleted from the database.")
    }

    // Delete a music
    fun deleteMusic(music: Music) {
        deleteLinkWithMusicId(music.id)
        val db = this.writableDatabase
        db.delete(TABLE_MUSIC, "$KEY_ID = ?", arrayOf(music.id.toString()))
        //db.close()

        Log.d(LOG, "Music ${music.fileName} deleted from the database.")
    }

    // Delete settings by id
    private fun deleteSettings(id: Int) {
        val db = this.writableDatabase
        db.delete(TABLE_SETTINGS, "$KEY_ID = ?", arrayOf(id.toString()))
        //db.close()

        Log.d(LOG, "Settings deleted from the database.")
    }

    // Delete the default settings
    fun deleteDefaultSettings(settings: Settings) {
        val db = this.writableDatabase
        db.delete(TABLE_SETTINGS, "$KEY_IS_DEFAULT > ?", arrayOf("0"))
        //db.close()

        Log.d(LOG, "Default settings deleted from the database.")
    }

    // Delete a link between a project and a music with the project id
    fun deleteLinkWithProjectId(idProject: Int) {
        val db = this.writableDatabase
        db.delete(TABLE_LINK_PROJECT_MUSIC, "$KEY_PROJECT_ID = ?", arrayOf(idProject.toString()))
        //db.close()
    }

    // Delete a link between a project and a music with the music id
    fun deleteLinkWithMusicId(idMusic: Int) {
        val db = this.writableDatabase
        db.delete(TABLE_LINK_PROJECT_MUSIC, "$KEY_MUSIC_ID = ?", arrayOf(idMusic.toString()))
        //db.close()
    }
}