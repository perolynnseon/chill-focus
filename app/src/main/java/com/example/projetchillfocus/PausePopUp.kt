package com.example.projetchillfocus

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment

class PausePopUp(var willFailIfStopped: Boolean) : DialogFragment() {

    public lateinit var main : MainActivity

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        main= activity as MainActivity
        return super.onCreateView(inflater, container, savedInstanceState)

    }


        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            var msg: String =""
            if(willFailIfStopped)
            {
                msg="\n\nYou didn't yet reach your goal. If you stop now, the session will be considered failed."
            }

        return activity?.let {
            // Use the Builder class for convenient dialog construction
            val builder = AlertDialog.Builder(it)
            builder.setMessage("Do you want to simply pause or end the session entirely?"+msg)
                .setIcon(R.drawable.icon)
                .setTitle("Pause Session")
                .setOnCancelListener() { dialog ->
                    main.Remove1Break()
                }
                .setNegativeButton("End",
                    DialogInterface.OnClickListener { dialog, id ->
                            main.endSession()
                    })

                .setPositiveButton("Pause",DialogInterface.OnClickListener { dialog, id ->
                        main.Remove1Break()

                })
                .setNeutralButton("cancel",
                    DialogInterface.OnClickListener { dialog, id ->

                        main.resumeSession()
                    })
            // Create the AlertDialog object and return it
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

}