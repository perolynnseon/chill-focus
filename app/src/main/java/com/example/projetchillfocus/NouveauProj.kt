package com.example.projetchillfocus

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity

class NouveauProj : AppCompatActivity(), AdapterView.OnItemSelectedListener{

    private lateinit var db: DatabaseHelper
    private lateinit var  models: ArrayList<Project>
    var globalClass = Global.applicationContext() as Global
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.nouveauproj)

        db = DatabaseHelper(applicationContext)
        models = db.getAllUnfinishedProjects()

        val spinner: Spinner = findViewById(R.id.colorpicker)
        spinner.onItemSelectedListener = this
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter.createFromResource(
            this,
            R.array.color_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }
    }

    fun cancel(view: View)
    {
        val intent = Intent(this@NouveauProj, MainActivity::class.java)
        startActivity(intent)
    }
    fun accept(view: View)
    {
        var projectName = findViewById<TextView>(R.id.inputprojname)
        var projectColor = findViewById<Spinner>(R.id.colorpicker)
        var projectDesc = findViewById<TextView>(R.id.newprojdesc)
        var projectSettings = findViewById<ToggleButton>(R.id.toggledefaultsettings)
        var imgId = 0


        if (projectName.text.toString() !="") {
            Toast.makeText(this, "Created project "+projectName.text.toString(), Toast.LENGTH_SHORT).show()
            when (projectColor.selectedItem.toString()) {
                "Red" -> imgId = R.drawable.tapered
                "Blue" -> imgId = R.drawable.tapeblue
                "Green" -> imgId = R.drawable.tapegreen
                "Orange" -> imgId = R.drawable.tapeorange
                "White" -> imgId = R.drawable.tapewhite
                else -> {
                    imgId = R.drawable.tape1
                }
            }
            val project = Project(0, projectName.text.toString(), projectColor.selectedItem.toString(), projectDesc.text.toString(),projectSettings.isChecked, imgId)
            db.addProject(project)

            val intent = Intent(this@NouveauProj, MainActivity::class.java)
            startActivity(intent)
        }
        else
            Toast.makeText(this, "Project has to have a name!", Toast.LENGTH_SHORT).show()
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {

    }

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }


}
