package com.example.projetchillfocus

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.content.Intent
import android.view.View


class Menu: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.menu)
    }

    fun GoToPlayer(view: View)
    {
        val intent = Intent(this@Menu, MainActivity::class.java)
        startActivity(intent)
    }

    fun GoToLibrary(view: View)
    {
        val intent = Intent(this@Menu, Bibliotheque::class.java)
        startActivity(intent)
    }
}