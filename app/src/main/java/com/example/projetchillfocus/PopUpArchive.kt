package com.example.projetchillfocus

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment

class PopUpArchive : DialogFragment() {

    public lateinit var main : MainActivity

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        main= activity as MainActivity
        return super.onCreateView(inflater, container, savedInstanceState)

    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            // Use the Builder class for convenient dialog construction
            val builder = AlertDialog.Builder(it)
            builder.setMessage("Do you want to archive (mark as finished) or permanently delete the project?\n\nArchived projects can be viewed, along side their stats, in the library.")
                .setIcon(R.drawable.icon)
                .setTitle("Archive Project")
                .setNegativeButton("Archive",
                    DialogInterface.OnClickListener { dialog, id ->
                        main.ArchiveProj()
                    })
                .setPositiveButton("Delete",DialogInterface.OnClickListener { dialog, id ->
                    main.DeleteProj()

                })
                .setNeutralButton("Cancel",
                    DialogInterface.OnClickListener { dialog, id ->

                        main.resumeSession()
                    })
            // Create the AlertDialog object and return it
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

}