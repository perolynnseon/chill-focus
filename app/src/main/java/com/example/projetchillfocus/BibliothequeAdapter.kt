package com.example.projetchillfocus

import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class BibliothequeAdapter(val projects: ArrayList<Project>):
    RecyclerView.Adapter<BibliothequeAdapter.ViewHolder>() {
    lateinit var acti:Bibliotheque
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val cardView: CardView = view.findViewById(R.id.finished_project_card)
        val textView: TextView = view.findViewById(R.id.finished_project_name)
        val item: RelativeLayout = view.findViewById(R.id.finished_project)
    }

    fun setActivity(act:Bibliotheque)
    {
        acti=act
    }

    // Create new views
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.finished_project_item, viewGroup, false)

        return ViewHolder(view)
    }

    // Replace the contents of a view
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.textView.text = projects[position].name
        var projectColor = projects[position].color
        var colorString = ""
        when(projectColor)
        {
            "Red" -> colorString = "#cd5c5c"
            "Blue" -> colorString = "#00bfff"
            "Green" -> colorString = "#00fa9a"
            "Orange" -> colorString = "#ff8c00"
            "White" -> colorString = "#f5f5f5"
        }
        viewHolder.cardView.setBackgroundColor(Color.parseColor(colorString))
        viewHolder.cardView.setOnClickListener() { PopUpStats(projects[position].id).show(acti.supportFragmentManager,"Stats") }
    }

    // Return the number of unfinished projects
    override fun getItemCount(): Int {
        return projects.size
    }
}